﻿using CharacterEditor.DataTypes;
using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CharacterEditor.IO
{
    public class FileParser
    {
        private List<GameEntity> _gameEntities = new List<GameEntity>();

        public List<GameEntity> GameEntities { get => _gameEntities; set => _gameEntities = value; }

        public void OpenFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Open Text File";
            openFileDialog.Filter = "TXT Files |*.txt";
            openFileDialog.InitialDirectory = Environment.CurrentDirectory;
            if ((bool)openFileDialog.ShowDialog())
            {
                try
                {
                    string file = File.ReadAllText(openFileDialog.FileName);
                    string[] entries = file.Split(new string[] { "new entry" }, StringSplitOptions.RemoveEmptyEntries);
                    char[] trim = new char[] { '\\', '\'', '\"', '\n', '\r', ' ' };

                    foreach (string entry in entries)
                    {
                        string newEntryValue;
                        string typeValue;
                        string usingValue;
                        string dataValueKeyValuePairs = string.Empty;
                        Dictionary<string, string> dataValues = new Dictionary<string, string>();

                        newEntryValue = entry.Split(new string[] { "type" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim(trim);
                        typeValue = entry.Split(new string[] { "type" }, StringSplitOptions.RemoveEmptyEntries)[1].Split(new string[] { "using" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim(trim);
                        usingValue = entry.Split(new string[] { "type" }, StringSplitOptions.RemoveEmptyEntries)[1].Split(new string[] { "using" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim(trim);
                        if (usingValue.StartsWith("data"))
                        {
                            dataValueKeyValuePairs = usingValue;
                            usingValue = string.Empty;

                            /*
                            data "Act part" ""
                            data "Flags" "KnockdownImmunity;PetrifiedImmunity;"
                            data "Talents" "AttackOfOpportunity
                            */
                        }
                        else
                        {
                            string temp = usingValue;
                            usingValue = temp.Split(new string[] { "data" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim(trim);
                            temp = temp.Replace(usingValue, string.Empty);
                            temp = temp.Trim(trim);
                            /*
                             * data "Act part" "4"
                             * data "Flags" "KnockdownImmunity;PetrifiedImmunity;PoisonImmunity"
                             * data "Talents" "AttackOfOpportunity
                             */
                            //string[] kvp = temp.Split(new string[] { "data" }, StringSplitOptions.RemoveEmptyEntries);
                            /*
                            "Act part" "4"
                            "Flags" "KnockdownImmunity;PetrifiedImmunity;PoisonImmunity"
                            "Talents" "AttackOfOpportunity
                            */
                            //foreach (string tmp in kvp)
                            //    dataValueKeyValuePairs += tmp;
                            dataValueKeyValuePairs = temp;
                        }
                        string[] kvps = dataValueKeyValuePairs.Split(new string[] { "data" }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < kvps.Length; i++)
                        {
                            string[] pair = kvps[i].Replace(" ", string.Empty).Split(trim, StringSplitOptions.RemoveEmptyEntries);
                            if (pair.Length > 1)
                                switch (pair[0])
                                {
                                    case "Actpart":
                                    case "Flags":
                                    case "Talents":
                                        dataValues.Add(pair[0], pair[0].Length > 1 ? pair[1] : "None");
                                        break;
                                    default:
                                        break;
                                }
                        }
                        Type type = Type.GetType("CharacterEditor.DataTypes." + newEntryValue.Replace(" ", "_"));
                        GameEntity gameEntity = (GameEntity)Activator.CreateInstance(type);
                        PropertyInfo[] propertyInfos = type.GetProperties();
                        Dictionary<string, int> deserializedDataValues = new Dictionary<string, int>();

                        
                        ActPart actPartInitVal = (ActPart)Enum.Parse(typeof(ActPart), "None");
                        Flags flagsInitVal = (Flags)Enum.Parse(typeof(Flags), "None");
                        Talents talentsInitVal = (Talents)Enum.Parse(typeof(Talents), "None");
                        Hashtable hashtable = new Hashtable();
                        hashtable.Add(nameof(ActPart), actPartInitVal);
                        hashtable.Add(nameof(Flags), flagsInitVal);
                        hashtable.Add(nameof(Talents), talentsInitVal);
                        foreach (KeyValuePair<string, string> keyValuePair in dataValues)
                        {
                            string[] enumValues = keyValuePair.Value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                            Type enumType = Type.GetType("CharacterEditor.DataTypes." + keyValuePair.Key.Replace("p", "P"));
                            if (enumType != null)
                            {
                                foreach (string enumValue in enumValues)
                                {
                                    if (keyValuePair.Key.Replace("p", "P") == nameof(ActPart))
                                    {
                                        hashtable[keyValuePair.Key] = (ActPart)Enum.Parse(typeof(ActPart), enumValue);
                                        //ActPart =  
                                    }
                                    //deserializedDataValues.Add(keyValuePair.Key, (int)Enum.Parse(typeof(ActPart), keyValuePair.Value));
                                    else if (keyValuePair.Key == nameof(Flags))
                                    {
                                        flagsInitVal |= (Flags)Enum.Parse(typeof(Flags), enumValue);
                                        hashtable[keyValuePair.Key] = flagsInitVal;
                                    }
                                    //deserializedDataValues.Add(keyValuePair.Key, (int)Enum.Parse(typeof(Flags), keyValuePair.Value.Replace(";", ",")));
                                    else if (keyValuePair.Key == nameof(Talents))
                                    {
                                        talentsInitVal |= (Talents)Enum.Parse(typeof(Talents), enumValue);
                                        hashtable[keyValuePair.Key] = talentsInitVal;
                                    }
                                    //deserializedDataValues.Add(keyValuePair.Key, (int)Enum.Parse(typeof(Talents), keyValuePair.Value));


                                }

                                deserializedDataValues.Add(keyValuePair.Key, (int)hashtable[keyValuePair.Key]);

                            }
                        }
                        PropertyInfo propertyInfo = propertyInfos.FirstOrDefault(x => x.Name == "Data");
                        if (propertyInfo != null)
                        {
                            SortedDictionary<string, int> propVal = (SortedDictionary<string, int>)propertyInfo.GetValue(gameEntity);
                            //foreach (KeyValuePair<string, int> keyValuePair in propVal)
                            //    if (dataValues.ContainsKey(keyValuePair.Key))
                            //    {
                            //        int r;
                            //        int.TryParse(dataValues[keyValuePair.Key], out r);
                            //        propVal[keyValuePair.Key] = r;
                            //    }
                            for(int i = 0; i < propVal.Count; i++)
                                if (deserializedDataValues.ContainsKey(propVal.Keys.ToList()[i]))
                                    propVal[propVal.Keys.ToList()[i]] = deserializedDataValues[propVal.Keys.ToList()[i]];

                            gameEntity.GetType().GetProperty(propertyInfo.Name).SetValue(gameEntity, propVal);
                        }

                        GameEntities.Add(gameEntity);
                    }

                }
                catch (Exception e)
                { }
            }
        }
    }
}
