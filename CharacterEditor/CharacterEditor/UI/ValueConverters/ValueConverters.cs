﻿using CharacterEditor.DataTypes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CharacterEditor.UI.ValueConverters
{
    public class ActPartConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // value is the flags enum.
            Type enumType = typeof(ActPart);

            // get all the possible values for an enum as ulong
            var allValues = from object arrayItem in Enum.GetValues(enumType)
                            select System.Convert.ToUInt64(arrayItem);

            // get all the flag values
            ulong setValues = System.Convert.ToUInt64(value);

            var result = from ulong singleValue in allValues
                         where (setValues & singleValue) == singleValue
                         select Enum.GetName(enumType, singleValue);
            return result;

            //return (ActPart)Enum.Parse(typeof(ActPart), value.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (int)Enum.Parse(typeof(ActPart), value.ToString());
        }
    }

    public class FlagsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // value is the flags enum.
            Type enumType = typeof(Flags);

            // get all the possible values for an enum as ulong
            var allValues = from object arrayItem in Enum.GetValues(enumType)
                            select System.Convert.ToUInt64(arrayItem);

            // get all the flag values
            ulong setValues = System.Convert.ToUInt64(value);

            var result = from ulong singleValue in allValues
                         where (setValues & singleValue) == singleValue
                         select Enum.GetName(enumType, singleValue);
            return result;
            //return (Flags)Enum.Parse(typeof(Flags), value.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (int)Enum.Parse(typeof(Flags), value.ToString());
        }
    }

    public class TalentsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // value is the flags enum.
            Type enumType = typeof(Talents);

            // get all the possible values for an enum as ulong
            var allValues = from object arrayItem in Enum.GetValues(enumType)
                            select System.Convert.ToUInt64(arrayItem);

            // get all the flag values
            ulong setValues = System.Convert.ToUInt64(value);

            var result = from ulong singleValue in allValues
                         where (setValues & singleValue) == singleValue
                         select Enum.GetName(enumType, singleValue);
            return result;
            //return (Talents)Enum.Parse(typeof(Talents), value.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (int)Enum.Parse(typeof(Talents), value.ToString());
        }
    }

    public class KeyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
