﻿using CharacterEditor.DataTypes;
using CharacterEditor.IO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CharacterEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private void OnPropertyChaged(object sender, PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(sender, e);
        }
        private ObservableCollection<GameEntity> _gameEntities;

        public MainWindow()
        {
            InitializeComponent();
            _gameEntities = new ObservableCollection<GameEntity>();
            DataContext = this;
        }

        public ObservableCollection<GameEntity> GameEntities
        {
            get => _gameEntities;
            set
            {
                if (_gameEntities != value)
                {
                    _gameEntities = value;
                    OnPropertyChaged(this, new PropertyChangedEventArgs("GameEntities"));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            FileParser fileParser = new FileParser();
            fileParser.OpenFile();

            foreach (GameEntity entity in fileParser.GameEntities)
                GameEntities.Add(entity);
        }

        private void ItemsControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
