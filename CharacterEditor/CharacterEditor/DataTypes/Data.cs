﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterEditor.DataTypes
{
    public enum ActPart
    {
        None,
        Zero,
        One,
        Two,
        Three,
        Four,
    };

    [Flags]
    public enum Flags
    {
        None                = 0x0,
        KnockdownImmunity   = 0x1,
        PetrifiedImmunity   = 0x2,
        PoisonImmunity      = 0x4,
        BleedImmunity       = 0x10,
        Indestructible      = 0x20,
    };

    [Flags]
    public enum Talents
    {
        None                    = 0x0,
        AttackOfOpportunity     = 0x1,
        Guerilla                = 0x2,
        WildBeast               = 0x4,
    };
}
