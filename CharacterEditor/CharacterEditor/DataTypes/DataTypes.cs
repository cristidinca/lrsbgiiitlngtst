﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterEditor.DataTypes
{
    #region Base_Types
    public abstract class GameEntity : INotifyPropertyChanged
    {
        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(sender, e);
        }
        
        protected internal GameEntity()
        {
           
        }

        private ActPart _actPArt = ActPart.None;
        private Flags _flags = Flags.None;
        private Talents _talents = Talents.None;

        public SortedDictionary<string, int> Data
        {
            get
            {
                return new SortedDictionary<string, int>
                {
                    { nameof(ActPart), (int)ActPArt },
                    { nameof(DataTypes.Flags), (int)Flags },
                    { nameof(DataTypes.Talents), (int)Talents }
                };
            }
            set
            {
                ActPArt = (ActPart)value[nameof(ActPart)];
                Flags = (Flags)value[nameof(DataTypes.Flags)];
                Talents = (Talents)value[nameof(DataTypes.Talents)];
                OnPropertyChanged(this, new PropertyChangedEventArgs("Data"));
            }
        }

        public string EntityName { get { return GetType().ToString(); } }

        public ActPart ActPArt { get => _actPArt; set { _actPArt = value; OnPropertyChanged(this,new PropertyChangedEventArgs("ActPArt")); } }
        public Flags Flags { get => _flags; set { _flags = value; OnPropertyChanged(this, new PropertyChangedEventArgs("Flags")); } }
        public Talents Talents { get => _talents; set { _talents = value; OnPropertyChanged(this, new PropertyChangedEventArgs("Talents")); } }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public abstract class Character : GameEntity
    {
        protected Character() : base() { }
    }

    public abstract class Consumable : GameEntity
    {
        protected Consumable() : base() { }
    }

    #endregion

    #region Characters
    #region  Base_Characters
    public class _Troll : Character
    {
        public _Troll() : base()
        {
            Data[nameof(ActPart)] = (int)ActPart.None;
            Data[nameof(Flags)] = (int)(Flags.KnockdownImmunity | Flags.PetrifiedImmunity);
            Data[nameof(Talents)] = (int)Talents.AttackOfOpportunity;
        }
    }

    public class _Orc : Character
    {
        public _Orc() : base()
        {
            Data[nameof(ActPart)] = (int)ActPart.Two;
            Data[nameof(Flags)] = (int)Flags.BleedImmunity;
            Data[nameof(Talents)] = (int)(Talents.Guerilla | Talents.WildBeast);
        }
    }

    #endregion Base_Characters

    public class Trolls_Grunt_Strong : _Troll
    {
        public Trolls_Grunt_Strong() : base()
        {
            Data["Flags"] |= (int)Flags.PoisonImmunity;
        }
    }

    public class Red_Ork : _Orc
    {
        public Red_Ork() : base()
        {
            Data[nameof(ActPart)] = (int)ActPart.Three;
            Data[nameof(Talents)] = (int)Talents.Guerilla;
        }
    }

    #endregion

    #region Consumables
    public class Health_Potion : Consumable
    {
        public Health_Potion() : base()
        {
            Data[nameof(Flags)] = (int)Flags.Indestructible;
        }
    }
    #endregion
}
